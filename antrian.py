from os import stat
from fastapi import FastAPI, Header
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from datetime import datetime
from typing import Optional, List
from random import randint
from logstash_async.handler import AsynchronousLogstashHandler
import requests
import logging
import sqlite3
import time

class AddAntrianRequest(BaseModel):
    tanggalAntri: datetime
    keluhan: str
    riwayatKesehatan: str

class UpdateAntrianRequest(BaseModel):
    tanggalAntri: Optional[datetime]
    status: Optional[str]
    dokter: Optional[str]
    keluhan: Optional[str]
    riwayatKesehatan: Optional[str]

class Antrian(BaseModel):
    antrianId: int
    tanggalAntri: datetime
    status: str
    dokter: str
    keluhan: str
    riwayatKesehatan: str

app = FastAPI()

conn = sqlite3.connect("antrian.db", check_same_thread = False)
cur = conn.cursor()

logger = logging.getLogger("python-logstash-logger")

@app.on_event("startup")
async def startup():
    seed()
    configure_logger()
    logger.info("Done opening connection with database")

@app.on_event("shutdown")
async def shutdown():
    cur.close()
    conn.close()
    logger.info("Done closing connection with database")

@app.post("/antrian/add")
def add_antrian(request: AddAntrianRequest, authorization: str = Header(None)):
    try:
        token = authorization.split()[-1]
        response = requests.get("https://law-auth-service.herokuapp.com/", headers = {"Token": token})
        if response.status_code != 200:
            return JSONResponse(status_code = 401, content = {"message": "Invalid/expired token"})

        cur.execute("SELECT * FROM antrian")
        antrianId = randint(0, 10000000)

        row = [antrianId, request.tanggalAntri, "Belum Diterima", None, request.keluhan, request.riwayatKesehatan, token]
        cur.execute("INSERT INTO antrian VALUES (?, ?, ?, ?, ?, ?, ?)", row)
        conn.commit()

        response = {
            "antrianId": antrianId,
            "tanggalAntri": request.tanggalAntri,
            "status": "Belum Diterima",
            "dokter": None,
            "keluhan": request.keluhan,
            "riwayatKesehatan": request.riwayatKesehatan
        }
        logger.debug(f"Antrian with id {antrianId} successfully added to the database")
        return response
    except:
        logger.error("Internal server error")
        return JSONResponse(status_code = 500, content = {"message": "Internal server error"})

@app.get("/antrian/list")
def list_antrian(response_model: List[Antrian], authorization: str = Header(None)):
    try:
        token = authorization.split()[-1]
        response = requests.get("https://law-auth-service.herokuapp.com/", headers = {"Token": token})
        if response.status_code != 200:
            return JSONResponse(status_code = 401, content = {"message": "Invalid/expired token"})

        cur.execute("SELECT * FROM antrian WHERE token = :token ORDER BY tanggalAntri DESC", [token])
        rows = cur.fetchall()
        conn.commit()

        responses = []
        for row in rows:
            responses.append({
                "antrianId": row[0],
                "tanggalAntri": row[1],
                "status": row[2],
                "dokter": row[3],
                "keluhan": row[4],
                "riwayatKesehatan": row[5]
            })
        logger.debug(f"All antrian of a patient successfully retrieved from the database")
        return responses
    except:
        logger.error("Internal server error")
        return JSONResponse(status_code = 500, content = {"message": "Internal server error"})
        
@app.post("/antrian/update/{antrian_id}")
def update_antrian(antrian_id, request: UpdateAntrianRequest, authorization: str = Header(None)):
    try:
        token = authorization.split()[-1]
        response = requests.get("https://law-auth-service.herokuapp.com/", headers = {"Token": token})
        if response.status_code != 200:
            return JSONResponse(status_code = 401, content = {"message": "Invalid/expired token"})

        data = {
            "antrianId": antrian_id,
            "tanggalAntri": request.tanggalAntri,
            "status": request.status,
            "dokter": request.dokter,
            "keluhan": request.keluhan,
            "riwayatKesehatan": request.riwayatKesehatan
        }
        cur.execute('''UPDATE antrian SET tanggalAntri = :tanggalAntri, status = :status, dokter = :dokter,
                       keluhan = :keluhan, riwayatKesehatan = :riwayatKesehatan WHERE antrianId = :antrianId''', data)
        conn.commit()

        logger.debug(f"Antrian with id {antrian_id} successfully updated in the database")
        return data
    except:
        logger.error("Internal server error")
        return JSONResponse(status_code = 500, content = {"message": "Internal server error"})

def seed():
    cur.execute('''CREATE TABLE IF NOT EXISTS antrian
                   (antrianId INTEGER PRIMARY KEY, tanggalAntri TEXT, status TEXT, dokter TEXT, keluhan TEXT, riwayatKesehatan TEXT, token TEXT)''')
    conn.commit()

def configure_logger():
    logger.setLevel(logging.DEBUG)

    host = "8ef166a3-7883-4013-a052-9bfaf2396397-ls.logit.io"
    port = 18893

    # Create the handler
    handler = AsynchronousLogstashHandler(host, port, database_path = '')

    # Create a format for log messages
    logging_format = '%(asctime)s.%(msecs)03d'
    logging_format += ' [%(levelname)s]'
    logging_format += ' %(message)s'
    logging_format += ' | %(name)s.%(module)s.%(funcName)s'
    date_format = '%Y-%m-%d | %H:%M:%S'
    formatter = logging.Formatter(logging_format, date_format)

    handler.setFormatter(formatter)

    logger.addHandler(handler)