FROM python:3.9

WORKDIR /app

COPY requirements.txt .
COPY antrian.py .

RUN pip install -r requirements.txt

CMD ["uvicorn", "antrian:app", "--host", "0.0.0.0", "--port", "8000"]
